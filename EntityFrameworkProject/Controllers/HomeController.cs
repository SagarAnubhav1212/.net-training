﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using EntityFrameworkProject.Models;
namespace EntityFrameworkProject.Controllers
{
    public class HomeController : Controller
    { 
        DemoEntities object1 = new DemoEntities();
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Index1()
        {
            var mds = object1.tblEmployees.ToList();
            return View(mds);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tblEmployee Emp)
        {
            if (ModelState.IsValid)
            {
                object1.tblEmployees.Add(Emp);
                object1.SaveChanges();
                return RedirectToAction("Index1");
            }
            return View(Emp);
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            tblEmployee Emp = object1.tblEmployees.Find(id);
            if (Emp == null)
            {
                return HttpNotFound();
            }
            return View(Emp);
        }
        public ActionResult Edit(int id)
        {
            tblEmployee Employee = object1.tblEmployees.Find(id);
            if (Employee == null)
            {
                return HttpNotFound();
            }
            return View(Employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tblEmployee emp)
        {
            if (ModelState.IsValid)
            {
                object1.Entry(emp).State = EntityState.Modified;
                object1.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emp);
        }

        public ActionResult Delete(int id)
        {
            var employee = object1.tblEmployees.Find(id);

            if (employee == null)
            {
                return View();
            }

            else
            {
                object1.tblEmployees.Remove(employee);
                object1.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}