﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DemoApplicationCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace DemoApplicationCore.Controllers
{
    public class ApiController : Controller
    {
        public IActionResult Index()
        {
            IEnumerable<Employee> emp = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:57041/weatherforecast");
                var responseTask = client.GetAsync("weatherforecast");//Controller Name
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readJob = result.Content.ReadAsAsync<IList<Employee>>();

                    readJob.Wait();
                    emp = readJob.Result;
                }
                else
                {
                    //return the error code here
                    emp = Enumerable.Empty<Employee>();
                    ModelState.AddModelError(string.Empty, "Error occured !");
                }

            }
            return View(emp);
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:57041/weatherforecast");
                var postTask = client.PostAsJsonAsync<Employee>("weatherforecast", employee);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(employee);
            }
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Employee model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:57041/weatherforecast");
                var responseTask = client.GetAsync("weatherforecast" + id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Employee>();
                    readTask.Wait();
                    model = readTask.Result;

                }
            }
            return View(model);
        }

        [HttpPut]
        public ActionResult Edit(Employee emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:57041/weatherforecast");
                var putTask = client.PutAsJsonAsync<Employee>("weatherforecast", emp);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(emp);
            }
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:57041/weatherforecast");
                var deleteTask = client.DeleteAsync("weatherforecast/" + id.ToString());
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }
    }
}