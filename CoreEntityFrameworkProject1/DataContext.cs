﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace CoreEntityFrameworkProject1.Models
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public System.Data.Entity.DbSet<Student> student { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"server = ANUBHAVSAGARR03\SQLEXPRESS;Database=Demo1;User Id=sa;Password=anubhav@123;Integrated Security=false;");
        }
    }
}
