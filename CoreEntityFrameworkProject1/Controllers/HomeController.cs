﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoreEntityFrameworkProject1.Models;

namespace CoreEntityFrameworkProject1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        DataContext Student = new DataContext();
        public ActionResult Index1()
        {
            var mds = Student.TestTable.ToList();
            return View(mds);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Student stu)
        {
            if (ModelState.IsValid)
            {
                stu.TestTable.Add(stu);
                stu.SaveChanges();
                return RedirectToAction("Index1");
            }
            return View(stu);
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            Student student = stu.TestTable.Find(id);
            if (stu == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        private ActionResult HttpNotFound()
        {
            throw new NotImplementedException();
        }

        public ActionResult Edit(int id)
        {
            Student student = stu.TestTable.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Student student)
        {
            if (ModelState.IsValid)
            {
                stu.Entry(student).State = EntityState.Modified;
                stu.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        public ActionResult Delete(int id)
        {
            var student = stu.TestTable.Find(id);

            if (student == null)
            {
                return View();
            }

            else
            {
                stu.TestTable.Remove(student);
                stu.SaveChanges();
                return RedirectToAction("Index");
            }
        }


    }
}
