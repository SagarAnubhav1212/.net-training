﻿using Microsoft.Diagnostics.Instrumentation.Extensions.Intercept;
using MVCDemoApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Userapi.Controllers
{
    public class WebapiController : ApiController
    {
        EmployeeDataAccessLayer Functions = new EmployeeDataAccessLayer();

        public IHttpActionResult GetAllRecord()
        {  
            return Ok(Functions.GetAllEmployees());
        }
        public IHttpActionResult GetAllRecord(int id)
        {
            return Ok(Functions.GetEmployeeData(id));
        }
        public IHttpActionResult PostEmployee(Employee employee)
        {
            Functions.AddEmployee(employee);
            return Ok();
        }

        public IHttpActionResult UpdateEmployee(Employee employee)
        {
            Functions.UpdateEmployee(employee);
            return Ok();
        }
        public IHttpActionResult DeleteEmployee(int id)
        {
            Functions.DeleteEmployee(id);
            return Ok();
        }
    }
}
