﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    [HandleError]
    public class DefaultController : Controller
    {

        // GET: Default
        [HandleError]
        public ActionResult DefaultIndex()
        {
            ViewBag.Name = "Anubhav Sagar";
            ViewBag.Age = 22;
            throw new Exception("This is unhandled exception");
            return View("DefaultIndex","_Layout");
        }

        //ContentResult Type
        [HandleError]
        public ContentResult Myindex()
        {
            throw new Exception("This is unhandled exception");
            return Content("<h3>This is Content Result type </h3>");
            
        }

        //Json Result
        [HandleError]
        public JsonResult json()
        {
            throw new Exception("This is unhandled exception");
            return Json(new { Name = "Anubhav Sagar", ID = 1 }, JsonRequestBehavior.AllowGet);
            
        }

        //RedirectResult
        [HandleError]
        public RedirectResult ReturnRedirect()
        {
            throw new Exception("This is unhandled exception");
            return Redirect("https://www.gmail.com");
            
        }

        //Partial View Result
        public PartialViewResult Index()
        {
            return PartialView("_SecondView");
        }
    }
}