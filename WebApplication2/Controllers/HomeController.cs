﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewData["FirstName"] = "Anubhav";
            ViewData["LastName"] = "Sagar";
            return View("Index","_LayoutPage1");
        }

        //Attribute Routing
        [Route("MVCtest")]
        public ActionResult MVCtest()
        {
            return View();
        }

        public ActionResult Tempsession()
        {
            ViewData["Firstname"] = "Deepak";
            ViewBag.Lastname = "Kumar";
            TempData["FullName"] = "Deepak Kumar";
            TempData["Address"] = "Taru Kunj Colony";
            return RedirectToAction("MyActionMethod");
        }

        public ActionResult MyActionMethod()
        {
            TempData.Keep();
            TempData.Keep();
            return View();
        }
    }
}