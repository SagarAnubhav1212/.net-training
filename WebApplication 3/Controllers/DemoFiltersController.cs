﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication_3.Action_Filters;
using static WebApplication_3.Action_Filters.MyLogActionFilter;

namespace WebApplication_3.Controllers
{
    [MyLogActionFilter]
    public class DemoFiltersController : Controller
    {
        // GET: DemoFilters
        [OutputCache(Duration = 15)]
        public string Index()
        {
            return "This is ASP.Net MVC Filters Tutorial";
        }

        [OutputCache(Duration = 20)]
        public string GetCurrentTime()
        {
            return DateTime.Now.ToString("T");
        }
    }
}