﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication_3.Models;

namespace WebApplication_3.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        
        public ActionResult Index(Cookies mycookies)
        {
            if (ModelState.IsValid == true)
            {
                HttpCookie cookie = new HttpCookie("MyCookies");
                cookie.Value = mycookies.name;
                HttpContext.Response.Cookies.Add(cookie);
            }
            return View();
        }
    }
}