﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication_3.Models
{
    
    public class Cookies
    {
        [Required]
        public string name { get; set; }
        public int studentId { get; set; }
    }
}