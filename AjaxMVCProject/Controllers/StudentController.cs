﻿using AjaxMVCProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AjaxMVCProject.Controllers
{
    public class StudentController : Controller
    {
        StudentContext context = new StudentContext();

        //Get Student Record:  
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult createStudent()
        {
            return View();
        }

        //Http Post Method For Creating the new record:
        [HttpPost]
        public ActionResult createStudent(Student std)
        {
            context.Students.Add(std);
            context.SaveChanges();
            string message = "SUCCESS";
            return Json(new { Message = message, JsonRequestBehavior.AllowGet });
        }

        //For Returning JSON Result Type List:
        public JsonResult getStudent(string id)
        {
            List<Student> students = new List<Student>();
            students = context.Students.ToList();
            return Json(students, JsonRequestBehavior.AllowGet);
        }
    }
}