﻿using System;

namespace ConsoleProject3
{
    class Program2
    {
        // Method for Calculating the Area  
        public double area(double r)
        {
            double area;
            area = 3.14 * Math.Pow(r, 2);
            return area;
        }
        // Method for Calculating the Perimeter 
        public double perimeter(double r)
        {
            double perimeter;
            perimeter = 2 * 3.14 * r;
            return perimeter;
        }
        static void Main(string[] args)
        {
            //Creation of Object

            Program2 myObj = new Program2();   
            Console.WriteLine("Press 1 : Perimeter of Circle , Press 2 : Area of Circle , Press 3 : Exit ");
            int choice = int.Parse(Console.ReadLine());
            Console.Write("Enter the radius of your circle : ");
            double r = Convert.ToDouble(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    var result1 = myObj.perimeter(r);
                    Console.WriteLine("The perimeter of yor circle : {0}", result1);
                    break;

                case 2:
                    var result2 = myObj.area(r);
                    Console.WriteLine("The Area of yor circle : {0}", result2);
                    break;

                default:
                    Console.WriteLine("Invalid Choice Entered");
                    break;
            }
            Console.ReadLine();
        }
    }
}
