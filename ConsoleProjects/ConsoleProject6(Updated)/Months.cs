﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject6
{
    class Months
    {
        int choice;
        int m;

        //Creation of Enum
        enum months
        {
            January,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public Months(int choice)
        {
            m = choice;
        }
        public void myMethod()
        {
            Dictionary<int, int> myDic = new Dictionary<int, int>
            {
            {(int)months.January,31},
            {(int)months.February,28},
            {(int)months.March,31},
            {(int)months.April,30 },
            {(int)months.May,31},
            {(int)months.June,30},
            {(int)months.July,31},
            {(int)months.August,31},
            {(int)months.September, 30},

            {(int)months.October,31},
            {(int)months.November,30},
            {(int)months.December,31}
        };
            foreach (KeyValuePair<int,int> result in myDic)
            {
                if (result.Key == m)
                {
                    Console.WriteLine("{0} Month have {1} days", result.Key, result.Value);
                }
            }
        }
        static void Main(string[] args)
        {
            Console.Write("Enter the Month :");
            int choice = Convert.ToInt32(Console.ReadLine());
            Months myobj = new Months(choice);
            myobj.myMethod();
        }
    }
}
