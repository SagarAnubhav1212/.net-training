﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject6
{
    // Interface Class 
    interface IPaiter
    {
        void Paint(); // Interface Method
    }

    interface  SecondInterface  // Second Interface Class
    {
        void ChangeSeatCover(); // Another Interface Method
    }
    class Vehicle : IPaiter , SecondInterface // Parent Class 
    {

        private String Vehiclename;
        private String color;

        public void Paint() // Interface Overridden Method 
        {
            Console.WriteLine("This is  First Interface Method");
        }

        public void ChangeSeatCover() // 2nd Interface Overridden Method 
        {
            Console.WriteLine("This Method is for Changing Vehicle SeatCover ");
        }

        // Property : 1 
        public String Vehiclecolor
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        // Enum Field 
        enum Noofwheels
        {
            Twowheeler= 2,
            FourWheeler = 4,
        }

        // Readonly Property :
        readonly int max_speed;

        // parents class parameterized constructor:
        public Vehicle(String Vehiclecolor,/* int wheels, */int max_speed)
        {
            //this.Vehiclename = Vehiclename;
            this.Vehiclecolor = color;
            //this.wheels = NoofWheels;
            this.max_speed = max_speed;
            Console.WriteLine(Vehiclecolor);
          //  Console.WriteLine(wheels);
            Console.WriteLine(max_speed);
        }
        public void Start()
        {
            Console.WriteLine(" Vehicle is Start");
        }
        public void Stop()
        {
            Console.WriteLine(" Vehicle is Stoppped");
        }

        public void SpeedUp(int x)
        {
            Console.WriteLine(" value of x is: " + x);
        }

        sealed class Car : Vehicle , IPaiter , SecondInterface // Child Class inherited the Vehicle Class
        {
            // Child class constructor 
            public Car(String Vehiclecolor,/* int wheels,*/ int max_speed) : base(Vehiclecolor,/* wheels,*/ max_speed)
            {
                Console.WriteLine("This is Car Class Constructor");
            }
            public void Calcualte_Total_Amount() //Method 
            {
                Console.WriteLine("This Method is For Calculating Total Amount : 1");
            }

            public void Paint() // Interface Overridden Method 
            {
                Console.WriteLine("This is First Interface Method");
            }

            public void ChangeSeatCover() // 2nd Interface Overridden Method 
            {
                Console.WriteLine("This Method is for Changing Vehicle SeatCover ");
            }

        }
        sealed class Bike : Vehicle  // Child Class : Bike Class Inherited the Vehicle Class  
        {
            public Bike(String Vehiclecolor, /*int wheels,*/ int max_speed) : base(Vehiclecolor,/* wheels,*/ max_speed)
            {
                Console.WriteLine("This is Bike Class Constructor");
            }
            public void Calcualte_Total_Amount12() //Method
            {
                Console.WriteLine("This Method is For Calculating Total Amount : 2 To check can we access the private class method");
            }

            public void Paint() // Interface Overridden Method 
            {
                Console.WriteLine("This is First Interface Method");
            }

            public void ChangeSeatCover() // 2nd Interface Overridden Method 
            {
                Console.WriteLine("This Method is for Changing Vehicle SeatCover ");
            }

        }
            // Main Method of this Class :
           /*  static void Main(string[] args)
           {
             int myvar =Convert.ToInt32(Noofwheels.Twowheeler);
            Console.WriteLine(myvar);

            // Creation of Vehicle Class Object and Then will call the  Interface Methods

            Car myObjCar = new Car("Yellow",100);
            myObjCar.Paint();
            myObjCar.ChangeSeatCover();
            Console.ReadKey();
            } */
     }
}
