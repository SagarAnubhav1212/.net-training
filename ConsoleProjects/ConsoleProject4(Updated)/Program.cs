﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleProject4
{
    class Vehicle // Parent Class 
    {
        
        private String Vehiclename;
        private string color;
        private int NoofWheels;

        // Property : 1 
        public String Vehiclecolor   
        {
            get
            { 
                return color;
            }  
            set 
            {
                color = value ;
            }
        }

        // Property Field :2
        public int wheels   
        {
            get
            {
                 return NoofWheels ;
            }
            set
            {
                NoofWheels = value;
            }
        }

        // Readonly Property :
        readonly int max_speed; 

        // parents class parameterized constructor:
        public Vehicle(String Vehiclecolor , int wheels , int max_speed)  
            {
            //this.Vehiclename = Vehiclename;
            this.Vehiclecolor = color;
            this.wheels = NoofWheels;
            this.max_speed = max_speed;
            Console.WriteLine(Vehiclecolor);
            Console.WriteLine(wheels);
            Console.WriteLine(max_speed);
        }
        public void Start()
        {
            Console.WriteLine(" Vehicle is Start");
        }
        public void Stop()
        {
            Console.WriteLine(" Vehicle is Stoppped");
        }

        public void SpeedUp(int x)
        {
            Console.WriteLine(" value of x is: " + x);
        }

       sealed class Car : Vehicle  // Child Class inherited the Vehicle Class
        {
            // Child class constructor 
            public Car(String Vehiclecolor , int wheels , int max_speed): base (Vehiclecolor, wheels,max_speed)
            {
                Console.WriteLine("This is Car Class Constructor");
            }
            public void Calcualte_Total_Amount() //Method 
            {
                Console.WriteLine("This Method is For Calculating Total Amount : 1");
            }

        }
       sealed class Bike : Vehicle  // Child Class : Bike Class Inherited the Vehicle Class  
        {
            public Bike(String Vehiclecolor, int wheels , int max_speed): base (Vehiclecolor, wheels , max_speed)
            {
                Console.WriteLine("This is Bike Class Constructor");
            }
            public void Calcualte_Total_Amount() //Method
            {
                Console.WriteLine("This Method is For Calculating Total Amount : 2");
            }
        }

        // Main Method of this Class :
        static void Main(string[] args)   
        {
           // Vehicle myObj = new Vehicle ("Green",14,160);
           
           // Car myObjCar = new Car("Yellow",4,60);
           // myObjCar.Vehiclecolor = "Red";
           // Console.WriteLine(myObjCar.Vehiclecolor);
           /* Console.WriteLine(myObjCar.Vehiclecolor);
            Console.WriteLine(myObjCar.wheels);
            Console.WriteLine(myObjCar.max_speed);*/

            Bike myObjbike = new Bike("Green",14,160);
            // Constructor Calling 
            //Console.WriteLine(myObjCar);
            // myObjCar.Calcualte_Total_Amount();
            // myObjbike.Calcualte_Total_Amount();

            // myObj.Vehiclename = "Bugatti";

            // myObj.Start(); // Method 1 Calling
            // myObj.Stop();  //Method 2  Calling
            // myObj.SpeedUp(10);
            //  myObj.wheels = 4;
            Console.ReadKey();
        }
    }
}
