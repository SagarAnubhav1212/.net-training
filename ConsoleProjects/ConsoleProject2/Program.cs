﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            string[] months = new string[]{ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

            // By using For Loop
            /* for (int i = 0;i<months.Length; i++)
                {
                    if (months[i][0] == 'J')
                        Console.WriteLine(months[i]);
                }
            */

            //By using For each Loop //
            /*
            foreach( var i in months[i])
            {
                    Console.WriteLine(months[i]);
            }
            */

            // by using While Loop
            /*
            while (i < months.Length)
            {
                if (months[i][0] == 'J')
                    Console.WriteLine(months[i]);
                i++;
            }
        }
    }
}
