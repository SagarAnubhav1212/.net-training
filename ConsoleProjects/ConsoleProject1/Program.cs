﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace First_Console_project
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            try
            {
                Console.Write("Enter your value: ");
                string Input = (Console.ReadLine());
                Console.WriteLine("Select the Choice from the listed Option ");
                Console.WriteLine("Press 1: Int , Press 2: Float , Press 3: Decimal Press 4: Bool , Press 5: Char , Press 6: Exit");
                int choice = int.Parse(Console.ReadLine());
                Console.WriteLine("You have Selected {0} option ",choice);

                switch (choice)
                {
                    case 1: Console.WriteLine(int.Parse(Input));
                        break;

                    case 2: Console.WriteLine(Convert.ToSingle(Input));
                        break;

                    case 3: Console.WriteLine(decimal.Parse(Input));
                        break;

                    case 4: Console.WriteLine(bool.Parse(Input));
                        break;

                    case 5: Console.WriteLine(Char.Parse(Input));
                        break; 

                    case 6: System.Environment.Exit(0);
                       // flag = false;
                        break;
                    default: Console.WriteLine("Invalid Choice");
                        break;
                }
               
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
