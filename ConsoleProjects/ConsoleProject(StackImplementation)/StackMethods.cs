﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleProjectStackImplementation
{
    public class StackMethods 
    {
        public List<object> list = new List<object>();
        public object object_val;

            public void Push(object obj)
            {
                object_val = obj;

                if (object_val == null)
                    throw new InvalidOperationException("Cannot use Push() if object is null");

            list.Insert(0, object_val);

        }
              public object Pop()
              {
                if (list.Count == 0)
                throw new InvalidOperationException("Stack is already Empty");

                object_val = list.FirstOrDefault();

                list.RemoveAt(0);

                return object_val;
              }

            public void Clear()
            {
                if (list.Count == 0)
                {
                throw new InvalidOperationException("Stack is already Empty");
                }    
                else
                {
                list.Clear();
                }       
            }

            public void Print()
            {
                if (list.Count == 0)
                    throw new InvalidOperationException("Stack is empty.");

                foreach (var s in list)
                {
                    Console.WriteLine(s);
                }
            }
        }
}
            