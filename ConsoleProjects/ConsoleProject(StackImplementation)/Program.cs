﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleProjectStackImplementation;

namespace ConsoleProject_StackImplementation_
{
    class Program
    {
        static void Main(string[] args)
        {
            StackMethods obj = new StackMethods();
            int flag = 1;
            
           while(flag==1)
            {
                Console.WriteLine("Enter Your Choice:\nEnter 1 For Push Operation\nEnter 2 For Pop Operation\nEnter 3 For Clear\nEnter 4 For Display\nEnter 5 For Exit : ");
                Console.WriteLine("------------------------------------------------------------------");
                string input = Console.ReadLine();
               // int input2 = Convert.ToInt32(input);
                int.TryParse(input, out int input2);
                try
                {
                    switch (input2)
                    {
                        case 1:

                            Console.WriteLine("Enter Your Values : ");
                            object userInput = Console.ReadLine();
                            obj.Push(userInput);
                            break;

                        case 2:
                            obj.Pop();
                            break;

                        case 3:
                            obj.Clear();
                            break;

                        case 4:
                            obj.Print();
                            break;

                        case 5:
                            flag = 0;
                            Environment.Exit(0);
                            break;

                        default:
                            Console.WriteLine("Invalid Entry !! Try Again");
                            break; 
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
