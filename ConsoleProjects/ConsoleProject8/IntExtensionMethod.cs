﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtentsionMethod
{
    public static class IntExtensionMethod
    {
        public static bool IsLessThan(this int i, int value)
        {
            return (i < value);
        }
    }
}
