﻿using System;
using ExtentsionMethod;

namespace ConsoleProject8
{
    class Program
    {
        static void Main(string[] args)
        {
         // Declaring the value of i as locally 
            Console.Write("Enter your Number : ");
            int i = Convert.ToInt32(Console.ReadLine());
            bool result = i.IsLessThan(100);
            Console.WriteLine("You have Entered the value: {0} and this Value is less than 100 so status: {1}",i,result);
           // Console.WriteLine("Your Value is Smaller than 100 So Status: {0}",result);
        }
    }
}
