﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject_StopWatch_
{
    class StopWatch
    {
        DateTime StartTime;
        DateTime StopTime;
        Boolean IsRunning;
        public void Start()
        {
            if (IsRunning)
            {
                throw new InvalidOperationException("Sorry you cannot start: Its already in running state");
            }
            StartTime = DateTime.Now;
            IsRunning = true;
        }

        public void Stop()
        {
            if (!IsRunning)
            {
                throw new InvalidOperationException("Sorry you cannot stop: Its already not in running state");
            }
            IsRunning = false;
            StopTime = DateTime.Now;
        }
        public TimeSpan TimeDuration()
        {
            if (IsRunning)
            {
                return DateTime.Now - StartTime;
            }
            else
            {
                return StopTime - StartTime;
            }
        }
    }
}
