﻿using System;

namespace ConsoleProject_StopWatch_
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            StopWatch myobj = new StopWatch();
            do
            {
                Console.WriteLine(" Please Select the Coices: 1-Start Timer 2-Stop Timer 3-Read Timer 0-Quit");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1: myobj.Start();
                        break;
                    case 2: myobj.Stop();
                        break;
                    case 3: Console.WriteLine(myobj.TimeDuration()); 
                        break;
                }
            } while (choice != 0);
        }
    }
}
