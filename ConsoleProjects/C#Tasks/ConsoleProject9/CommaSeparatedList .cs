﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject9
{
   public class CommaSeparatedList
    {
        public void SmallestNo()
        {
			var Mylist = new List<int>();
			while (true)
			{
				Console.WriteLine("Enter Your Numbers with Comma Separated:");
				var input = Console.ReadLine();

				var sortarray = input.Split(',');

				if ((sortarray.Length == 0) || (sortarray.Length < 5))
				{
					Console.WriteLine("Invalid list! Try again");
				}
				else
				{
					foreach (var number in sortarray)
						Mylist.Add(Convert.ToInt32(number));
						break;
				}
			}
			Mylist.Sort();
			Console.WriteLine("Your smallest three No are");
			for (int i = 0; i < 3; i++)
			{
				Console.WriteLine("{0}", Mylist[i]);
			}
		}
	}
}
