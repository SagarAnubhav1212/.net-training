﻿using System;
using System.Collections.Generic;
using ConsoleProject9;

namespace ConsoleProject9
{
    public class Program
    {
        static void Main(string[] args)
        {
            int choice;
            do
            {
                Console.WriteLine("Enter Your Choice \n 1: To Add the unique No. in List \n 2: Display the Unique No in List \n 3: Display the Smallest 3 No. \n 4: Calcuate the Vowels in String \n 5: For Exit");
                string input = Console.ReadLine();
                choice = Convert.ToInt32((input));
                switch (choice)
                {
                    case 1: Console.WriteLine("Enter Your Unique 5 Numbers :");
                        UniqueNoEntry obj2 = new UniqueNoEntry();
                        obj2.UniqueNoChecking();
                        break;

                    case 2:
                        DisplayList obj3 = new DisplayList();
                        obj3.DisplayUniqueNo();
                        break;

                    case 3:
                        CommaSeparatedList obj4 = new CommaSeparatedList();
                        obj4.SmallestNo();
                        break;

                    case 4:
                        Console.Write("Enter Your String:");
                        string myStr = Console.ReadLine();
                        Vowels obj = new Vowels();
                        obj.VowelsCount(myStr);
                        break;
                }
            } while (choice != 5);
        }
    }
}