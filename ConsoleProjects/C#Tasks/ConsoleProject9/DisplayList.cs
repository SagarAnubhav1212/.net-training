﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject9
{
	public class DisplayList
	{
		public void DisplayUniqueNo()
		{
			 var List = new List<int>();

			while (true)
			{
				Console.WriteLine("Enter a number or type Quit For Exit");
				var input = Console.ReadLine();

				if (input.CompareTo("Quit") == 0)
					break;
				else
				{
					int Number = Convert.ToInt32(input);
					if (List.Contains(Number))
						continue;
					else
						List.Add(Number);
				}
			}
			Console.WriteLine("Your Unique No are: ");
			foreach (var output in List)
			{
				Console.WriteLine("{0}", output);
			}
		}
	}
}
