﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject9
{
    public class UniqueNoEntry
    {
        public void UniqueNoChecking()
        {
            var inputNo = new int[5];
           // Console.WriteLine("Enter Your Unique 5 Numbers :");

            for (int i = 0; i < 5; i++)
            {
                while (true)
                {
                    var newValue = Convert.ToInt32(Console.ReadLine());
                    var currentNumber = Array.IndexOf(inputNo, newValue);
                    if (currentNumber == -1)
                    {
                        inputNo[i] = newValue;
                        break;
                    }
                    Console.WriteLine("Please Wait !!You already entered that number,Re-try");
                }
            }
            Array.Sort(inputNo);
            Console.WriteLine();

            Console.WriteLine("Your Unique 5 No. in a List :");
            foreach (var n in inputNo)
                Console.WriteLine(n);

            //Console.ReadLine();
        }
    }
}
