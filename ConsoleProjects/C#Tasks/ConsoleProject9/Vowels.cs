﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject9
{
    public class Vowels
    {
        public void VowelsCount(string myStr)
        {
            int i, len, vowel_count;
            vowel_count = 0;
            len = myStr.Length;
            for (i = 0; i < len; i++)
            {
                if (myStr[i] == 'a' || myStr[i] == 'e' || myStr[i] == 'i' || myStr[i] == 'o' || myStr[i] == 'u' || myStr[i] == 'A' || myStr[i] == 'E' || myStr[i] == 'I' || myStr[i] == 'O' || myStr[i] == 'U')
                {
                    vowel_count++;
                }
            }
             int result = Convert.ToInt32(vowel_count);
             Console.Write("\nVowels Contains in your string: {0}\n",result);
             
        }
    }
}
