--Common Table Expression--
Select * From Employee

With Employee_cte (Emp_Id,First_Name,Last_Name) AS
(
	Select Emp_Id,First_Name,Last_Name From  Employee
	Where Emp_Id is Not Null
)
Select Count(Emp_Id) As 'No of Employee' From Employee_cte

--Stored Procedure--

Use Demo;
GO
Create Procedure EmployeespGetEmployeeDetails
	@FirstName varchar(100),
	@LastName varchar(100),
	@age int,
AS
	
	Select First_Name,Last_Name,Age From Employee
	Where First_Name = @FirstName And Last_Name= @LastName And Age=@age;

Go
EXECUTE EmployeespGetEmployeeDetails N'Anubhav',N'Sagar',21; 

--View --
Create view Employee_Record As
Select First_Name,Last_Name,Age,City From Employee
Where City='Meerut';
--For Accessing the view--
SELECT * FROM Employee_Record;

--User Defined Function--
CREATE FUNCTION ufn_EmployeeRecord(@Emp_Id int)
RETURNS TABLE
AS
RETURN
(
    Select  Employee.First_Name,Employee.Last_Name,Employee.Age, Department.Dept_Name,Department.Resident_Address
	From Department,Employee
    Where Department.Dept_Id = Employee.Emp_Id
);
GO
SELECT * FROM ufn_EmployeeRecord(10);