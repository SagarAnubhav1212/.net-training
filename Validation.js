
	$(document).ready(function()
	{
		var fname_err = true;
		var lname_err = true;
		var emailid_err = true;
		var pNo_err = true;
		var address_err = true;	
		

		$('#fname').keyup(function()
		{
			
		fusername_check();
		});
		
		function fusername_check()
		{	
			var f_val =$('#fname').val();

			if(f_val.length == '')
			{
			$('#err_fName').show();
			$('#err_fName').html("*First Name is Required*");
			$('#err_fName').css("color","red");
			fname_err = false;
			return false;
			}
			else
			{
			$('#err_fName').hide();
			}
			
			if((f_val.length <3) || (f_val.length >50))
			{
			$('#err_fName').show();
			$('#err_fName').html("*name must be between 3 to 50 characters*");
			$('#err_fName').css("color","red");
			fname_err = false;
			return false;
			}
			else
			{
			$('#err_fName').hide();
			}
			
			if(!isNaN(f_val))
			{
			$('#err_fName').show();
			$('#err_fName').html("*Only Alphabets are allowed*");
			$('#err_fName').css("color","red");
			fname_err = false;
			return false;
			}
			else
			{
			$('#err_fName').hide();
			}
		}
		
		$('#lname').keyup(function()
		{
			lusername_check();
		});	
		
		function lusername_check()
		{	
			var l_val =$('#lname').val();

			if(l_val.length == '')
			{
			$('#err_lName').show();
			$('#err_lName').html("*Last Name is Required*");
			$('#err_lName').css("color","red");
			lname_err = false;
			return false;
			}
			else
			{
			$('#err_lName').hide();
			}
			
			if((l_val.length <3) || (l_val.length >50))
			{
			$('#err_lName').show();
			$('#err_lName').html("* must be between 3 to 50 characters*");
			$('#err_lName').css("color","red");
			lname_err = false;
			return false;
			}
			else
			{
			$('#err_lName').hide();
			}
			
			if(!isNaN(l_val))
			{
			$('#err_lName').show();
			$('#err_lName').html("*Only Alphabets are allowed*");
			$('#err_lName').css("color","red");
			lname_err = false;
			return false;
			}
			else
			{
			$('#err_lName').hide();
			}
			
		}
		
		$('#email_Id').keyup(function()
		{
			email_check();
		});
		
		function email_check()
		{	
			var u_mail =$('#email_Id').val();

			if(u_mail.length == "")
			{
			$('#err_emailId').show();
			$('#err_emailId').html("*Email id is Required*");
			$('#err_emailId').css("color","red");
			emailid_err = false;
			return false;
			}
			else
			{
			$('#err_emailId').hide();
			}
			
			if(u_mail.indexOf('@') <=0 )
			{
			$('#err_emailId').show();
			$('#err_emailId').html("*Email Format is Incorrect");
			$('#err_emailId').css("color","red");
			emailid_err = false;
			return false;
			}
			else
			{
			$('#err_emailId').hide();
			}
			
			if((u_mail.charAt(u_mail.length - 4)!='.') && (u_mail.charAt(u_mail.length - 3)!='.'))
			{
			$('#err_emailId').show();
			$('#err_emailId').html("*Email Format is Incorrect*");
			$('#err_emailId').css("color","red");
			emailid_err = false;
			return false;
			}
			else
			{
			$('#err_emailId').hide();
			}
		}

		$('#pNo').keyup(function()
		{
		pNo_check();
		});	
			
		function pNo_check()
		{	
			var p_no =$('#pNo').val();

			if(p_no.length == "")
			{
			$('#err_pNo').show();
			$('#err_pNo').html("*Mobile No is required*");
			$('#err_pNo').css("color","red");
			pNo_err= false;
			return false;
			}
			else
			{
			$('#err_pNo').hide();
			}
			
			if(isNaN(p_no))
			{
			$('#err_pNo').show();
			$('#err_pNo').html("*Only Digits are required*");
			$('#err_pNo').css("color","red");
			pNo_err= false;
			return false;
			}
			else
			{
			$('#err_pNo').hide();
			}
			
			if(p_no.length !=10)
			{
			$('#err_pNo').show();
			$('#err_pNo').html("*Phone No must be of 10 digits*");
			$('#err_pNo').css("color","red");
			pNo_err= false;
			return false;
			}
			else
			{
			$('#err_pNo').hide();
			}
		}
			
		$('#address').keyup(function()
		{
			address_check();
		});
		
		function address_check()
		{	
			var u_add =$('#address').val();

			if(u_add.length == "")
			{
			$('#err_address').show();
			$('#err_address').html("*Address is Required*");
			$('#err_address').css("color","red");
			address_err = false;
			return false;
			}
			else
			{
			$('#err_address').hide();
			}
		}
	});
		
		function validation()
		{
			
		var status = true;
		var fName=document.getElementById("fname").value;
		var lName=document.getElementById("lname").value;
		var emailid=document.getElementById("email_Id").value;
		var phoneNo=document.getElementById("pNo").value;
		//var gender=document.getElementById("gender").value;
		var education=document.getElementById("higherEducation").value;
		//var eduaction_value=education.options[higherEducation.selectedIndex].value;//
		var address=document.getElementById("address").value;

		if(fName == "")
		{
		document.getElementById('err_fName').innerHTML=" First Name is required";
		document.getElementById('err_fName').style.color="red";
		document.getElementById('fname').focus();
		status = false;
		}
		else if(fName.length <=2 || fname.length>50)
		{
		document.getElementById('err_fName').innerHTML="Only 50 characters are allowed";
		status = false;
		}
		else if(!isNaN(fName))
		{
		document.getElementById('err_fName').innerHTML="Only Alphabets are allowed";
		status = false;
		}
		
		
		if(lName == "")
		{
		document.getElementById('err_lName').innerHTML="Last Name is required";
		document.getElementById('err_lName').style.color="red";
		document.getElementById('lname').focus();
		status = false;
		}
		else if(lName.length <=2 || lName.length>50)
		{
		document.getElementById('err_lName').innerHTML="Only 50 characters are allowed";
		status = false;
		}
		else if(!isNaN(lName))
		{
		document.getElementById('err_lName').innerHTML="Only Alphabets are allowed";
		status = false;
		}

		
		if(emailid == "")
		{
		document.getElementById('err_emailId').innerHTML="Email Id is required";
		document.getElementById('err_emailId').style.color="red";
		document.getElementById('email_Id').focus();
		status = false;
		}
		else if(emailid.indexOf('@') <=0 )
		{
		document.getElementById('err_emailId').innerHTML="Incorrect Foramt of Email";
		status =  false;
		}
		else if((emailid.charAt(emailid.length - 4)!='.') && (emailid.charAt(emailid.length - 3)!='.'))
		{
		document.getElementById('err_emailId').innerHTML="Email is not valid ";
		status = false;
		}
		
	
		if(phoneNo == "")
		{	
		document.getElementById('err_pNo').innerHTML="Phone No is required";
		document.getElementById('err_pNo').style.color="red";
		document.getElementById('pNo').focus();
		
		status = false;
		}
		else if(isNaN(phoneNo))
		{	
		document.getElementById('err_pNo').innerHTML="Only digits are required";
		status = false;
		}
		else if(phoneNo.length !=10)
		{	
		document.getElementById('err_pNo').innerHTML="Mobile No must be 10 digits only";
		
		status = false;
		}

	/*	if(gender == "")
		{
		document.getElementById('err_gender').innerHTML="Please Select your Gender";
		
		status = false;
		}
	
		
		if(education == "")
		{
		document.getElementById('err_education').innerHTML="Please Select Your Highest Education";
		
		status = false;
		}
	*/
		if(address == "")
		{
		document.getElementById('err_address').innerHTML="Address is required";
		document.getElementById('err_address').style.color="red";
		document.getElementById('address').focus();
		status = false;
		}
		return status;
	}
	
	var selectedRow = null;
	function Check_Validation()
	{
		var cv= validation();
		if(cv)
		{
			var formData = readFormData();
			if(selectedRow == null)
			{
			AddRow(formData);
			selectedRow = null;
			ResetForm();
			return false;
			}
			else
			{
				updateRecord(formData);
				selectedRow = null;
				return false;
			}
		}
		
	}
	
	
		var list1 = [];
		var list2 = [];
		var list3 = [];
		var list4 = [];
		var list5 = [];
		var list6 = [];
		var list7 = [];
		var list8 = [];
		var list9 = [];
		var del ='<input type = "button" value="Delete" onclick="Deletebtn(this)"/>';
		var ed = '<input type = "button" value="Edit" onclick="revertedit(this)"/>';

     

	var n = 1;
	var x = 0;
	
	// For inserting new record in table //

	function AddRow()
	{				

		var AddRown = document.getElementById('show');
		var NewRow = AddRown.insertRow(n);
		

			list1[x] = document.getElementById("fname").value;
			list2[x] = document.getElementById("middleName").value;
			list3[x] = document.getElementById("lname").value;
			list4[x] = document.getElementById("email_Id").value;
			list5[x] = document.getElementById("pNo").value;
			var gen = document.getElementsByName("gender");
			for(var i=0;i<gen.length;i++)
			{
			if(gen[i].checked)
			{
			list6[x] = gen[i].value;
			}
			}
			
			
			list7[x] = document.getElementById("higherEducation").value;
			if( document.getElementById("sub").checked)
			{
				list8[x]="yes"
			}
			else
			{
				list8[x] ="no";
			}
			
			list9[x] = document.getElementById("address").value;

			var cel1 = NewRow.insertCell(0);
			var cel2 = NewRow.insertCell(1);
			var cel3 = NewRow.insertCell(2);
			var cel4 = NewRow.insertCell(3);
			var cel5 = NewRow.insertCell(4);
			var cel6 = NewRow.insertCell(5);
			var cel7 = NewRow.insertCell(6);
			var cel8 = NewRow.insertCell(7);
			var cel9 = NewRow.insertCell(8);
			var cel10 = NewRow.insertCell(9);
			var cell11 = NewRow.insertCell(10);

			cel1.innerHTML = list1[x];
			cel2.innerHTML = list2[x];
			cel3.innerHTML = list3[x];
			cel4.innerHTML = list4[x];
			cel5.innerHTML = list5[x];
			cel6.innerHTML = list6[x];
			cel7.innerHTML = list7[x];
			cel8.innerHTML = list8[x];
			cel9.innerHTML = list9[x];
			cel10.innerHTML= del;
			cell11.innerHTML = ed;

		n++;
		x++;
	}
	
	function readFormData()
	{
		var formData = {};
		formData["fname"] = document.getElementById("fname").value;
		formData["middleName"] = document.getElementById("middleName").value;
		formData["lname"] = document.getElementById("lname").value;
		formData["email_Id"] = document.getElementById("email_Id").value;
		formData["pNo"] = document.getElementById("pNo").value;
	//	formData["gender"] = document.getElementById("gender").value;
		formData["higherEducation"] = document.getElementById("higherEducation").value;
		formData["sub"] = document.getElementById("sub").value;
		formData["address"] = document.getElementById("address").value;
		return formData;
	}	
	
	// For Making field reset//
	
	function ResetForm()
	{
		document.getElementById("fname").value = "";
		document.getElementById("middleName").value = "";
		document.getElementById("lname").value = "";
		document.getElementById("email_Id").value = "";
		document.getElementById("pNo").value = "";
		//document.getElementById("gender").value = "";
		document.getElementById("higherEducation").value = "";
		document.getElementById("sub").value = "";
		document.getElementById("address").value = "";
	}
	
// For updating the Field if tables 
function updateRecord(formData)
{
	selectedRow.cells[0].innerHTML=document.getElementById("fname").value;
	selectedRow.cells[1].innerHTML=document.getElementById("middleName").value;
	selectedRow.cells[2].innerHTML=document.getElementById("lname").value;
	selectedRow.cells[3].innerHTML=document.getElementById("email_Id").value;
	selectedRow.cells[4].innerHTML=document.getElementById("pNo").value;

	var gen=document.getElementsByName("gender");
	for(var i=0;i<gen.length;i++)
	{
		if(gen[i].checked)
		{
		selectedRow.cells[5].innerHTML=gen[i].value;
		}
	}
	selectedRow.cells[6].innerHTML=document.getElementById("higherEducation").value;

	if(document.getElementById("sub").checked==true)
	{
	selectedRow.cells[7].innerHTML="yes";
	}
	else
	{
		selectedRow.cells[7].innerHTML="no";
	}
	selectedRow.cells[8].innerHTML=document.getElementById("address").value;

}
	
	// for deleting the records //
	
	function Deletebtn(td)
	{
	row=td.parentElement.parentElement;
	document.getElementById("show").deleteRow(row.rowIndex);
	}
	
	//For Editing the fields //
function revertedit(td)
{
	selectedRow=td.parentElement.parentElement;
	document.getElementById("fname").value=selectedRow.cells[0].innerHTML;
	document.getElementById("middleName").value=selectedRow.cells[1].innerHTML;
	document.getElementById("lname").value=selectedRow.cells[2].innerHTML;
	document.getElementById("email_Id").value=selectedRow.cells[3].innerHTML;
	document.getElementById("pNo").value=selectedRow.cells[4].innerHTML;
	var y = document.getElementsByName("gender");
	for(var i=0;i<y.length;i++)
	{
	  
    if(y[i].value == selectedRow.cells[5].innerHTML)
	{
      
      document.getElementById(y[i].id).checked=true;
    }

	}
 
  document.getElementById("higherEducation").value=selectedRow.cells[6].innerHTML;
  var b=document.getElementById("yes");
  
    if(selectedRow.cells[7].innerHTML == "Yes")
	{
      document.getElementById("sub").checked=true;
	}
  document.getElementById("address").value=selectedRow.cells[8].innerHTML;
  
}
	
	
	
	
		