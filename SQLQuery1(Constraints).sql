--Creating the New Table Department and Using Constraints concepts-----

Create table Department
(
Dept_Id int Primary key,
Dept_Name varchar(100) Not Null,
Emp_Phone_No int Unique,
Resident_address varchar(100) Not NUll
);

Select * From Department;

Insert Into Department values(2,'C#',1234567891,'xYZ')
Insert Into Department values(3,'C#',1234567892,'xYZ')
Insert Into Department values(4,'C#',1234567893,'xYZ')
Insert Into Department values(5,'.Net',1234567490,'xYZ')
Insert Into Department values(6,'.Net',1234567590,'xYZ')
Insert Into Department values(7,'.Net',1234567690,'xYZ')
Insert Into Department values(8,'Node',1234567790,'xYZ')
Insert Into Department values(9,'Node',1234567620,'xYZ')
Insert Into Department values(10,'Node',1234567990,'xYZ')
