-- Where Clause Query ---------------------------------------------------

Select * From Employee where Emp_Id = 10;

Select First_Name,Last_Name,City From Employee where Emp_Id = 7;

Select First_Name,Last_Name,City From Employee where age >= 20;

Select * From Employee where First_Name Like '%av';

Select First_Name,Last_Name,age From Employee where City= 'Meerut';
--------------------------------------------------------------------------
--Order by Queries--

Select * From Employee Order by City;

Select * From Employee Order by Emp_Id Asc;

Select * From Employee Order by Emp_Id desc;

Select * From Employee Order by Emp_Id desc , City Asc ;
--------------------------------------------------------------------------
--Group By Queries--------------------------------------------------------
Select * From Employee;

SELECT COUNT(Emp_Id),First_Name FROM Employee GROUP BY First_Name;

Select * From Employee;

SELECT COUNT(Emp_Id), City FROM Employee GROUP BY City ORDER BY COUNT(Emp_Id) DESC;

---------------------------------------------------------------------------

--Having Clause Queries-----------------------------------------------------


SELECT COUNT(Emp_Id), First_Name FROM Employee
GROUP BY First_Name
HAVING COUNT(Emp_Id) = 1;

SELECT Max(Emp_Id), First_Name, Age FROM Employee
GROUP BY First_Name, Age
HAVING COUNT(Emp_Id)=1;

---------------------------------------------------------------------
--Distinct Queries-------

Select * From Employee;

Select Distinct City from Employee;

SELECT COUNT(DISTINCT City) FROM Employee;
-------------------------------------------------------------------
--Like Clause Queries------

Select * From Employee where First_Name Like '%bh%';

Select * From Employee where First_Name Like 'v%';

Select * From Employee where First_Name Like '%an';

Select * From Employee where First_Name Like '_r%';

Select * From Employee where First_Name Like 'a%n';
-------------------------------------------------------------
--In Clause Queries------

SELECT * FROM Employee WHERE City IN ('Meerut');

SELECT * FROM Employee WHERE City Not IN ('Meerut');


