--String Functions--

SELECT First_Name, ASCII(First_Name) AS NumCodeOfFirstChar FROM Employee;

SELECT CONCAT(First_Name, ' ', Last_Name) AS Address FROM Employee;

SELECT Lower (First_Name)  FROM Employee;

SELECT Upper (First_Name) FROM Employee;

Select Reverse (First_Name) From Employee;

SELECT substring(First_Name, 2, 5) AS ExtractString FROM Employee;

--------------------------------------------------------------------------------

--Maths Functions 

SELECT ABS(-243.5);

SELECT AVG(Age) AS AverageAge FROM Employee;

SELECT CEILing(20.45);

SELECT FLOOR(25.75);

SELECT MAX(AGe) AS LargestAge FROM Employee;

SELECT Min(AGe) AS SmallestAge FROM Employee;

SELECT Power(4, 2);

SELECT ROUND(135.375, 2);

SELECT SQRT(64);

SELECT SUM(AGE) FROM Employee;

----------------------------------------------------------
--DateTime Function

SELECT DATEADD(DAY,10 ,'2017-06-15');

SELECT DATEADD(Month,4 ,'2017-06-15');

SELECT DATEADD(MINUTE,15,'2017-06-15 09:34:21');

SELECT DATEDIFF(Year,'2017-06-15','2020-12-09');

SELECT DATEDIFF(DAYOFYEAR,'2017-06-15','2020-12-09');

SELECT GETDATE();

SELECT DAYOFWEEK('2017-06-15');

SELECT MONTHNAME('2017-06-15');

SELECT CONVERT( DATE,'2017-08-29');

SELECT CONVERT( DATETIME,'2017-08-29');

SELECT CONVERT(TIME,'2017-08-29');

SELECT CONVERT(CHAR,'234')

SELECT CONVERT(INT,'234')

SELECT CONVERT(TIME,'14:06:10');

