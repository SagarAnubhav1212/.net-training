﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace LinqProgram1
{
    public class Student
     {
         public int StudentID { get; set; }
         public String StudentName { get; set; }
         public int age { get; set; }
     }

    
   class Program
   {
        // Bool type delegate
        delegate bool Isteenager(Student stud);
        static void Main(string[] args)
        {

            string[] games = { "Cricket", "Volleyball", "Football", "Hockey" };

            var myLinqQuery = from Game in games where Game.Contains('e') select Game;

            foreach (var name in myLinqQuery)
                Console.WriteLine(name + " ");

            //Linq Query Syntax
            IList<string> stringList = new List<string>()
           {
           "C# Tutorials",
           "VB.NET Tutorials",
           "Learn C++",
           "MVC Tutorials" ,
           "Java"
           };
            var result = from s in stringList where s.Contains("Tutorials") select s;
            foreach (var name in result)
                Console.WriteLine(name + " ");



            Student[] studentArray = {
                   new Student() { StudentID = 1, StudentName = "John", age = 18 } ,
                   new Student() { StudentID = 2, StudentName = "Steve",  age = 21 } ,
                   new Student() { StudentID = 3, StudentName = "Bill",  age = 25 } ,
                   new Student() { StudentID = 4, StudentName = "Ram" , age = 20 } ,
                   new Student() { StudentID = 5, StudentName = "Ron" , age = 31 } ,
                   new Student() { StudentID = 6, StudentName = "Chris",  age = 17 } ,
                   new Student() { StudentID = 7, StudentName = "Rob",age = 19  } ,
               };

            // Use LINQ to find teenager students
            Student[] teenAgerStudents = studentArray.Where(s => s.age > 12 && s.age < 20).ToArray();
            foreach (var name in teenAgerStudents)
            {
                Console.WriteLine(name.StudentID);
                Console.WriteLine(name.StudentName);
                Console.WriteLine(name.age);
            }


            // Use LINQ to find first student whose name is Bill 
            Student bill = studentArray.Where(s => s.StudentName == "Bill").FirstOrDefault();
            Console.WriteLine(bill.age);


            // Use LINQ to find student whose StudentID is 5
            Student student5 = studentArray.Where(s => s.StudentID == 5).FirstOrDefault();
            Console.WriteLine(student5.StudentName);

            Console.WriteLine();
            //Lambda Expression

            Isteenager isTeenAger = delegate (Student s) { return s.age > 12 && s.age < 20; };
            Student stud = new Student() { age = 18 };
            Console.WriteLine("Result of Lambda Exppression:" + isTeenAger(stud));

            Console.WriteLine();

            //Assigning Lambda Expression to Delegate
            Func<Student, bool> isStudentTeenAger = s => s.age > 12 && s.age < 20;
            Student st = new Student() { age = 19 };
            Console.WriteLine(isStudentTeenAger(st));

            Console.WriteLine();

            //Action Delegate 
            Action<Student> PrintStudentDetail = s => Console.WriteLine("Name: {0}, Age: {1} ", s.StudentName, s.age);
            Student std = new Student()
            {
                StudentName = "John",
                age = 18
            };
            PrintStudentDetail(std);

            // Student collection
            IList<Student> studentList = new List<Student>() {
                new Student() { StudentID = 1, StudentName = "John", age = 13} ,
                new Student() { StudentID = 2, StudentName = "Moin", age = 21 } ,
                new Student() { StudentID = 3, StudentName = "Bill", age = 18 } ,
                new Student() { StudentID = 4, StudentName = "Ram" , age = 20} ,
                new Student() { StudentID = 5, StudentName = "Ron" , age = 15 }
            };
            //Func Delegate in LINQ Method Syntax
            Func<Student, bool> StudentTeenAger = s => s.age > 12 && s.age < 20;

            var teenAgerStudent = studentList.Where(StudentTeenAger);

            Console.WriteLine("Teen age Students:");

            foreach (Student sd in teenAgerStudent)
            {
                Console.WriteLine(sd.StudentName);

            }

            Console.WriteLine();


            //Func Delegate in LINQ Query Syntax
            Func<Student, bool> StudentTeenAger1 = s => s.age > 12 && s.age < 20;

            var teenAgerStudents1 = from s in studentList
                                   where isStudentTeenAger(s)
                                   select s;

            Console.WriteLine("Teen age Students:");

            foreach (Student stde in teenAgerStudents)
            {
                Console.WriteLine(stde.StudentName);
            }

        }
    }
}
