﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Linq;

namespace LinqProgram1
{
	public class Students
	{
		public int StudentID { get; set; }
		public string StudentName { get; set; }
		public int Age { get; set; }

		class LINQOperators
		{
			public static void Main(string[] args)
			{
				IList mixedList = new ArrayList();
				mixedList.Add(0);
				mixedList.Add("One");
				mixedList.Add("Two");
				mixedList.Add(3);
				mixedList.Add(new Student() { StudentID = 1, StudentName = "Bill" });

				//OfType Operator
				var stringResult = from s in mixedList.OfType<string>() select s;

				var intResult = from s in mixedList.OfType<int>() select s;

				var stdResult = from s in mixedList.OfType<Student>() select s;

				foreach (var str in stringResult)
					Console.WriteLine(str);

				foreach (var integer in intResult)
					Console.WriteLine(integer);

				foreach (var std in stdResult)
					Console.WriteLine(std.StudentName);

				Console.WriteLine();

				//Sorting:OrderBy and OrderBydescending

				IList<Students> studentList = new List<Students>() //Student Collection
				{
				new Students() { StudentID = 1, StudentName = "John", Age = 18 } ,
				new Students() { StudentID = 2, StudentName = "Steve",  Age = 15 } ,
				new Students() { StudentID = 3, StudentName = "Bill",  Age = 25 } ,
				new Students() { StudentID = 4, StudentName = "Ram" , Age = 20 } ,
				new Students() { StudentID = 5, StudentName = "Ron" , Age = 18 }
				};

				var orderByResult = from s in studentList orderby s.StudentName select s;

				var orderByDescendingResult = from s in studentList orderby s.StudentName descending select s;
				Console.WriteLine("Ascending Order:");

				foreach (var std in orderByResult)
					Console.WriteLine(std.StudentName);

				Console.WriteLine();

				Console.WriteLine("Descending Order:");
				foreach (var std in orderByDescendingResult)
					Console.WriteLine(std.StudentName);

				Console.WriteLine();

				//GroupBy Operator
				// GroupBy in Query Syntax 
				var groupedResult = from s in studentList group s by s.Age;
				foreach (var ageGroup in groupedResult)
				{
					Console.WriteLine("Age Group: {0}", ageGroup.Key);

					foreach (Students s in ageGroup)
					{
						Console.WriteLine("Student Name: {0}", s.StudentName);
					}
				}

				//GroupBy in Method Syntax
				var groupedResult1 = studentList.GroupBy(s => s.Age);
				foreach (var ageGroup in groupedResult)
				{
					Console.WriteLine("Age Group: {0}", ageGroup.Key);

					foreach (Students s in ageGroup)
						Console.WriteLine("Student Name: {0}", s.StudentName);
				}

				//Select Operator

				//Select Operator in Query Syntax
				var selectResult = from s in studentList select s.StudentName;

				foreach (var name in selectResult)
				{
					Console.WriteLine(name);
				}

				//Select Operator in Method Syntax

				var selectResult1 = studentList.Select(s => new {Name = s.StudentName, Age = s.Age});
				foreach (var item in selectResult1)
					Console.WriteLine("Student Name: {0}, Age: {1}", item.Name, item.Age);

				//Quantifier Operators: All,Any,Contains

				//1.All Operator
				bool areAllStudentsTeenAger = studentList.All(s => s.Age > 12 && s.Age < 20);
				Console.WriteLine(areAllStudentsTeenAger);

				//2.Any Operator
				bool isAnyStudentTeenAger = studentList.Any(s => s.Age > 12 && s.Age < 20);
				Console.WriteLine(isAnyStudentTeenAger);

				//3.Contains Operator 
				IList<int> intList = new List<int>() { 1, 2, 3, 4, 5 };
				bool result = intList.Contains(1);
				Console.WriteLine(result);

				//Aggregate Oprators:Average,Count,Sum,Max

				//1.Aggregate Operator:
				IList<String> strList = new List<String>() { "One", "Two", "Three", "Four", "Five" };
				var commaSeperatedString = strList.Aggregate((s1, s2) => s1 + ", " + s2);
				Console.WriteLine(commaSeperatedString);

				//2.Average Operator:
				IList<int> intList1 = new List<int>() { 10, 20, 30 };
				var avg = intList1.Average();
				Console.WriteLine("Average: {0}", avg);

				IList<Students> studentLists = new List<Students>() 
				{
				new Students() { StudentID = 1, StudentName = "John", Age = 13 } ,
				new Students() { StudentID = 2, StudentName = "Moin",  Age = 21 } ,
				new Students() { StudentID = 3, StudentName = "Bill",  Age = 18 } ,
				new Students() { StudentID = 4, StudentName = "Ram" , Age = 20 } ,
				new Students() { StudentID = 5, StudentName = "Ron" , Age = 15 }
				};
				var avgAge = studentList.Average(s => s.Age);
				Console.WriteLine("Average Age of Student: {0}", avgAge);

				//3.Count Oprerator

				var totalStudents = studentLists.Count();
				Console.WriteLine("Total Students: {0}", totalStudents);
				var adultStudents = studentLists.Count(s => s.Age >= 18);
				Console.WriteLine("Number of Adult Students: {0}", adultStudents);

				//4.Max Operator
				var oldest = studentLists.Max(s => s.Age);
				Console.WriteLine("Oldest Student Age: {0}", oldest);

				//5.Min Operator
				var smallest = studentLists.Min(s => s.Age);
				Console.WriteLine("Youngest Student Age : {0}", smallest);

				//6.Sum Operator
				var sumOfAge = studentLists.Sum(s => s.Age);
				Console.WriteLine("Sum of all student's age: {0}", sumOfAge);

				//Element Operators
				//ElementAt
				IList<int> intList2 = new List<int>() { 10, 21, 30, 45, 50, 87 };
				IList<string> strList2 = new List<string>() { "One", null, "Three", "Four", "Five" };


				Console.WriteLine("1st Element in intList: {0}", intList2.ElementAt(0));
				Console.WriteLine("1st Element in strList: {0}", strList2.ElementAt(0));

				Console.WriteLine("2nd Element in intList: {0}", intList2.ElementAt(1));
				Console.WriteLine("2nd Element in strList: {0}", strList2.ElementAt(1));

				Console.WriteLine("3rd Element in intList: {0}", intList2.ElementAtOrDefault(2));
				Console.WriteLine("3rd Element in strList: {0}", strList2.ElementAtOrDefault(2));

				Console.WriteLine("10th Element in intList: {0} - default int value", intList2.ElementAtOrDefault(9));
				Console.WriteLine("10th Element in strList: {0} - default string value (null)", strList2.ElementAtOrDefault(9));

				//First, FirstorDefault Operators
				IList<int> intList3 = new List<int>() { 7, 10, 21, 30, 45, 50, 87 };
				IList<string> strList3 = new List<string>() { null, "Two", "Three", "Four", "Five" };

				Console.WriteLine("1st Element in intList: {0}", intList3.First());
				Console.WriteLine("1st Element in strList: {0}", strList3.First());

				//FirstorDefault
				Console.WriteLine("1st Element in strList: {0}", strList3.FirstOrDefault());
				Console.WriteLine("1st Element in intList: {0}", intList3.FirstOrDefault());

				//Last operator
				Console.WriteLine("1st Element in intList: {0}", intList3.Last());
				Console.WriteLine("1st Element in strList: {0}", strList3.Last());

				//LastorDefault
				Console.WriteLine("1st Element in intList: {0}", intList3.LastOrDefault());
				Console.WriteLine("1st Element in strList: {0}", strList3.LastOrDefault());

				//Single & SingleorDefault
				IList<int> oneElementList = new List<int>() { 7 };
				Console.WriteLine("The only element in oneElementList: {0}", oneElementList.Single());
				Console.WriteLine("The only element in oneElementList: {0}",oneElementList.SingleOrDefault());

				//Concatentaion Operator
				IList<string> collection1 = new List<string>() { "One", "Two", "Three" };
				IList<string> collection2 = new List<string>() { "Five", "Six" };
				
				var concateResult = collection1.Concat(collection2);
				foreach (string str in concateResult)
					Console.WriteLine(str);

				//SequenceEqual
				IList<string> strList1 = new List<string>() { "One", "Two", "Three", "Four", "Three" };
				IList<string> strList4 = new List<string>() { "One", "Two", "Three", "Four", "Three","Six"};

				bool isEqual = strList1.SequenceEqual(strList4);
				Console.WriteLine(isEqual);//It will return False

				//Generation Operators
				//DefaultIfEmpty
				IList<int> emptyList = new List<int>();
				var newList1 = emptyList.DefaultIfEmpty();
				var newList2 = emptyList.DefaultIfEmpty(10);

				Console.WriteLine("Count: {0}", newList1.Count());
				Console.WriteLine("Value: {0}", newList1.ElementAt(0));

				Console.WriteLine("Count: {0}", newList2.Count());
				Console.WriteLine("Value: {0}", newList2.ElementAt(0));

				//Empty Operator
				var emptyCollection1 = Enumerable.Empty<string>();
				var emptyCollection2 = Enumerable.Empty<Student>();

				Console.WriteLine("Type: {0}", emptyCollection1.GetType().Name);
				Console.WriteLine("Count: {0}", emptyCollection1.Count());

				Console.WriteLine("Type: {0}", emptyCollection2.GetType().Name);
				Console.WriteLine("Count: {0}", emptyCollection2.Count());

				//Range Operator
				var intCollection = Enumerable.Range(1, 10);
				Console.WriteLine("Total Count: {0} ", intCollection.Count());
				for (int i = 0; i < intCollection.Count(); i++)
					Console.WriteLine("Value at index {0} : {1}", i, intCollection.ElementAt(i));

				//Repeat Operator 
				var intCollection1 = Enumerable.Repeat(10, 10);
				Console.WriteLine("Total Count: {0} ", intCollection1.Count());

				for (int i = 0; i < intCollection1.Count(); i++)
					Console.WriteLine("Value at index {0} : {1}", i, intCollection1.ElementAt(i));

				//Set Operatos: Distinct,Except,Intersect,Union

				//Distinct
				IList<string> strList5 = new List<string>() { "One", "Two", "Three", "Two", "Three" };
				IList<int> intList5 = new List<int>() { 1, 2, 3, 2, 4, 4, 3, 5 };
				var distinctList1 = strList5.Distinct();

				foreach (var str in distinctList1)
					Console.WriteLine(str);
				var distinctList2 = intList5.Distinct();
				foreach (var i in distinctList2)
					Console.WriteLine(i);

				//Except
				IList<string> strList6 = new List<string>() { "One", "Two", "Three", "Four", "Five" };
				IList<string> strList7 = new List<string>() { "Four", "Five", "Six", "Seven", "Eight" };
				var result1 = strList6.Except(strList7);
				foreach (string str in result1)
					Console.WriteLine(str);

				//Intersect
				var result2 = strList6.Intersect(strList7);
				foreach (string str in result2)
					Console.WriteLine(str);

				//Union
				var result3 = strList6.Union(strList7);
				foreach (string str in result3)
					Console.WriteLine(str);

			}
		}
	}
}
