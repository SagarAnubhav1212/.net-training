﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;
using System.Linq.Expressions;

namespace LinqProgram1
{
    public class Students
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
    public static class EnumerableExtensionMethods
    {
        public static IEnumerable<Students> GetTeenAgerStudents(this IEnumerable<Students> source)
        {

            foreach (Students std in source)
            {
                Console.WriteLine("Accessing student {0}", std.StudentName);

                if (std.Age > 12 && std.Age < 20)
                    yield return std;
            }
        }
    }
    class LINQOperation
    {

        static void ReportTypeProperties<T>(T obj)
        {
            Console.WriteLine("Compile-time type: {0}", typeof(T).Name);
            Console.WriteLine("Actual type: {0}", obj.GetType().Name);
        }
        static void Main(string[] args)
        {

            //Partitioning Operators:
            //Skip Operator
            IList<string> strList = new List<string>() { "One", "Two", "Three", "Four", "Five" };
            var result = strList.Skip(1);

            foreach (var str in result)
                Console.WriteLine(str);

            //SkipWhile Operator
            IList<string> strList1 = new List<string>() { "One", "Two", "Three", "Four", "Five", "Six" };
            var result1 = strList1.SkipWhile(s => s.Length < 4);

            foreach (string str in result1)
                Console.WriteLine(str);
            Console.WriteLine();
            //Take Operator
            var newList = strList1.Take(2);

            foreach (var str in newList)
                Console.WriteLine(str);
            Console.WriteLine();
            //TakeWhile Operator
            IList<string> strList2 = new List<string>() { "Three", "Four", "Five", "Hundred" };

            var newList2 = strList2.TakeWhile(s => s.Length > 4);
            foreach (var str in newList2)
                Console.WriteLine(str);

            //Conversion Operator
            //AsEnumerable & AsQueryable
            Students[] studentArray = {
                    new Students() { StudentID = 1, StudentName = "John", Age = 18 } ,
                    new Students() { StudentID = 2, StudentName = "Steve",  Age = 21 } ,
                    new Students() { StudentID = 3, StudentName = "Bill",  Age = 25 } ,
                    new Students() { StudentID = 4, StudentName = "Ram" , Age = 20 } ,
                    new Students() { StudentID = 5, StudentName = "Ron" , Age = 31 } ,
                };

            ReportTypeProperties(studentArray);
            ReportTypeProperties(studentArray.AsEnumerable());
            ReportTypeProperties(studentArray.AsQueryable());
            Console.WriteLine();
            //Cast Operator
            ReportTypeProperties(studentArray);
            ReportTypeProperties(studentArray.Cast<Students>());

            //ToList,ToArray
            Console.WriteLine("strList type: {0}", strList1.GetType().Name);

            string[] strArray = strList1.ToArray<string>();// converts List to Array

            Console.WriteLine("strArray type: {0}", strArray.GetType().Name);

            IList<string> newList1 = strArray.ToList<string>(); // converts array into list

            Console.WriteLine("newList type: {0}", newList1.GetType().Name);

            //ToDictionary
            IList<Students> studentList1 = new List<Students>() {
                    new Students() { StudentID = 1, StudentName = "John", Age = 18 } ,
                    new Students() { StudentID = 2, StudentName = "Steve",  Age = 21 } ,
                    new Students() { StudentID = 3, StudentName = "Bill",  Age = 18 } ,
                    new Students() { StudentID = 4, StudentName = "Ram" , Age = 20 } ,
                    new Students() { StudentID = 5, StudentName = "Ron" , Age = 21 }
                };
            IDictionary<int, Students> studentDict = studentList1.ToDictionary<Students, int>(s => s.StudentID);

            foreach (var key in studentDict.Keys)
                Console.WriteLine("Key: {0}, Value: {1}", key, (studentDict[key] as Students).StudentName);
            Console.WriteLine();

            //Expression
            Expression<Func<Students, bool>> isTeenAgerExpr = s => s.Age > 12 && s.Age < 20;
        
            Func<Students, bool> isTeenAger = isTeenAgerExpr.Compile();
            //Invoke
            bool result2 = isTeenAger(new Students() { StudentID = 1, StudentName = "Steve", Age = 20 });
            Console.WriteLine(result2);

            //Expression Tree
            ParameterExpression pe = Expression.Parameter(typeof(Students), "s");

            MemberExpression me = Expression.Property(pe, "Age");

            ConstantExpression constant = Expression.Constant(18, typeof(int));

            BinaryExpression body = Expression.GreaterThanOrEqual(me, constant);

            var ExpressionTree = Expression.Lambda<Func<Students, bool>>(body, new[] { pe });


            Console.WriteLine("Expression Tree: {0}", ExpressionTree);

            Console.WriteLine("Expression Tree Body: {0}", ExpressionTree.Body);

            Console.WriteLine("Number of Parameters in Expression Tree: {0}", ExpressionTree.Parameters.Count);

            Console.WriteLine("Parameters in Expression Tree: {0}", ExpressionTree.Parameters[0]);


            //Deferred Execution
            var teenAgerStudents = from s in studentList1.GetTeenAgerStudents()
                                   select s;
            foreach (Students teenStudent in teenAgerStudents)
                Console.WriteLine("Student Name: {0}", teenStudent.StudentName);
            Console.WriteLine();
            //Let Keyword
            var lowercaseStudentNames = from s in studentList1
                                        let lowercaseStudentName = s.StudentName.ToLower()
                                        where lowercaseStudentName.StartsWith("r")
                                        select lowercaseStudentName;
            foreach (var name in lowercaseStudentNames)
                Console.WriteLine(name);
            Console.WriteLine();
            //Into Keyword
            var teenAgerStudent1 = from s in studentList1  where s.Age > 12 && s.Age < 20 select s into teenStudents where teenStudents.StudentName.StartsWith("B") select teenStudents;
            foreach (Students std in teenAgerStudent1)
            {
                Console.WriteLine(std.StudentName);
            }
        }
    }
}
