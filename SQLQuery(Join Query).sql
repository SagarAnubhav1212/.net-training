--Join Queries--

--Inner Join---
SELECT Person.First_Name,Employee.Age
FROM Person
INNER JOIN  Employee on Person.Person_Id=Employee.Emp_Id;

SELECT Person.First_Name,Employee.Age,Employee.City
FROM Person
INNER JOIN  Employee on Person.Person_Id=Employee.Emp_Id;

--Left Outer Join --

SELECT Employee.First_Name, Person.First_Name
FROM Employee
LEFT JOIN Person ON Employee.Emp_Id = Person.Person_Id order by Employee.Emp_Id

--Right Join--

SELECT Person.Person_Id, Employee.First_Name, Employee.Last_Name
FROM Person
RIGHT JOIN Employee ON Person.Person_Id = Employee.Emp_Id order by Person.Person_Id;

--Full Join--

SELECT Employee.First_Name, Person.Person_Id
FROM Employee
FULL OUTER JOIN Person ON Employee.Emp_Id = Person.Person_Id
ORDER BY Employee.First_Name;



