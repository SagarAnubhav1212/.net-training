
--Implementing the Primary Key and Foreign Key Concepts----
ALter table Employee Add Dept_Id int Foreign Key references Department(Dept_Id);

Alter table Employee Add Phone_No varchar(10);

Select * From Employee;

Update Employee Set Dept_Id = 2 where Emp_Id=2;
Update Employee Set Dept_Id = 3 where Emp_Id=3;
Update Employee Set Dept_Id = 4 where Emp_Id=4;
Update Employee Set Dept_Id = 5 where Emp_Id=5;
Update Employee Set Dept_Id = 6 where Emp_Id=6;
Update Employee Set Dept_Id = 7 where Emp_Id=7;
Update Employee Set Dept_Id = 8 where Emp_Id=8;
Update Employee Set Dept_Id = 9 where Emp_Id=9;
Update Employee Set Dept_Id = 10 where Emp_Id=10;


Update Employee Set Phone_No ='1234567891' where Emp_Id=3;
Update Employee Set Phone_No ='1234567891' where Emp_Id=4;
Update Employee Set Phone_No ='1234567891' where Emp_Id=5;
Update Employee Set Phone_No ='1234567891' where Emp_Id=6;
Update Employee Set Phone_No ='1234567891' where Emp_Id=7;
Update Employee Set Phone_No ='1234567891' where Emp_Id=8;
Update Employee Set Phone_No ='1234567891' where Emp_Id=9;
Update Employee Set Phone_No ='1234567891' where Emp_Id=10;

Select  Employee.First_Name,Employee.Last_Name,Employee.Age, Department.Dept_Name,Department.Resident_Address From Department,Employee
Where Department.Dept_Id = Employee.Emp_Id; 


