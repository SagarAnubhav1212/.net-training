﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoApplicationCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DempApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        //[HttpGet]
        //public IEnumerable<WeatherForecast> Get()
        //{
        //    var rng = new Random();
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateTime.Now.AddDays(index),
        //        TemperatureC = rng.Next(-20, 55),
        //        Summary = Summaries[rng.Next(Summaries.Length)]
        //    })
        //    .ToArray();
        //}
        DataAccessLayer dr = new DataAccessLayer();
        [System.Web.Http.HttpGet]
        [HttpGet]
        public IActionResult GetAllRecord()
        {
            return Ok(dr.GetAllEmployees());
        }
        //[HttpGet]
        public IActionResult GetAllRecord(int id)
        {
            return Ok(dr.GetEmployeeData(id));
        }
        [HttpPost]
        public IActionResult PostEmployee(Employee employee)
        {
           dr.AddEmployee(employee);
           return Ok(dr);
        }
        [HttpPut]
        public IActionResult PutEmployee(Employee employee)
        {
           dr.UpdateEmployee(employee);
           return Ok();
        }
        [HttpDelete]
        public IActionResult DeleteEmployee(int id)
        {
            dr.DeleteEmployee(id);
            return Ok();
        }
    }
}
